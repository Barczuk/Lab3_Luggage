package ie.ucd.luggage;

public class Bomb implements Item {

	public Bomb() {
	}

	private String type = "Bomb";
	private boolean dangerous = true;

	public String getType() {
		return type;
	}

	public double getWeight() {
		double weight = 5.0;
		return weight;
	}

	public boolean isDangerous() {

		return this.dangerous;
	}

}
