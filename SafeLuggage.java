package ie.ucd.luggage;

// import java.util.List;

// import java.util.ArrayList;

public class SafeLuggage extends Luggage {

	private String passwordAttempt;

	public SafeLuggage(String passwordAttempt) {
		this.passwordAttempt = passwordAttempt;
	}

	double MaxWeight = 10.0;
	double BagWeight = 1.5;
	String CorrectPassword = "4637";

	public double getBagWeight() {
		return BagWeight;
	}

	public double getMaxWeight() {
		return MaxWeight;
	}

	// At every user attempt to add an item the password is checked.
	// If correct password is provided, the item is not dangerous and the max total
	// weight is not exceeded, the item will be added.

	public void add(Item item) {

		if (CorrectPassword.equals(passwordAttempt)) {
			System.out.println("Password correct. Checking item safety.");
			boolean danger = item.isDangerous();
			item.isDangerous();
			if (danger) {
				System.out.println("The item you are trying to add is dangerous. Access denied.\n");
			} else {
				double items = this.getWeight();
				if (items + item.getWeight() < this.getMaxWeight()) {
					super.add(item);
					System.out.println(item.getType() + " added successfully. Current weight of your luggage is " + this.getWeight() +" kg.\n" );
				} else {
					System.out.println("You can't add this item; max allowabale weight of your bag would be exceeded.\n");
				}
			}
		} else {
			System.out.println("Password incorrect: no items have been added.\n");
		}
	}

	// If correct password is provided, user can remove chosen item from the bag.

	public void removeItem(int index) {

		if (CorrectPassword.equals(passwordAttempt)) {
			super.removeItem(index);
			System.out.println("Password correct: item removed successfully.\n");

		} else {
			System.out.println("Password incorrect: no items have been removed.\n");
		}
	}
}