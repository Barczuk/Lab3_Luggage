package ie.ucd.luggage;

public class Suitcase extends Luggage {

	private double bagWeight;
	private double maxWeight = 10.0;

	public Suitcase(double bagWeight) {
		this.bagWeight = bagWeight;
	}

	public double getBagWeight() {
		return bagWeight;
	}

	public double getMaxWeight() {
		return this.maxWeight;
	}

}
