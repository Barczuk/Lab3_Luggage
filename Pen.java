package ie.ucd.luggage;

public class Pen implements Item {

	public Pen() {
	}

	private String type = "Pen";
	private boolean dangerous = false;

	public String getType() {
		return type;
	}

	public double getWeight() {
		double weight = 0.05;
		return weight;
	}

	public boolean isDangerous() {
		return this.dangerous;
	}

}
