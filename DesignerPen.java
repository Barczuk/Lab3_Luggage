package ie.ucd.luggage;

public class DesignerPen extends Pen {
	public DesignerPen(String brand) {
		super();
		this.brand = brand;
	}

	private String brand;

	public String getType() {
		return "Designer Pen " + this.brand;
	}
}
