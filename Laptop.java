package ie.ucd.luggage;

public class Laptop implements Item {

	public Laptop() {
	}

	private String type = "Laptop";
	private boolean dangerous = false;

	public String getType() {
		return type;
	}

	public double getWeight() {
		double weight = 2.5;
		return weight;
	}

	public boolean isDangerous() {
		return this.dangerous;
	}
}