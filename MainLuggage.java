package ie.ucd.luggage;
// import java.util.*;

public class MainLuggage {
	public static void main(String[] args) {

		Item myBomb = new Bomb();
		Item myPen = new Pen();
		Item myLaptop = new Laptop();
		Item myDesignerPen = new DesignerPen("Parker");

		
		// Case 1: Correct password, attempt to add a dangerous item
		System.out.print("Case 1: adding a dangerous item.\n");
		SafeLuggage SafeCase1 = new SafeLuggage("4367");
		SafeCase1.add(myBomb);
		SafeCase1.add(myPen);
		SafeCase1.add(myLaptop);
		SafeCase1.removeItem(1);
		SafeCase1.add(myDesignerPen);
		SafeCase1.add(myLaptop);
		System.out.print("\n\n");

		
		// Case 2: Correct password, attempt to add items that exceed the max weight
		SafeLuggage SafeCase2 = new SafeLuggage("4637");
		System.out.print("Case 2: exceedng max weight.\n");
		SafeCase2.add(myPen);
		SafeCase2.add(myLaptop);
		SafeCase2.add(myDesignerPen);
		SafeCase2.add(myLaptop);
		SafeCase2.add(myLaptop);
		SafeCase2.add(myLaptop);
		SafeCase2.add(myLaptop);
		System.out.print("\n\n");

		
		// Case 3: Incorrect password, attempt to add any items should fail
		SafeLuggage SafeCase3 = new SafeLuggage("1234");
		System.out.print("Case 3: incorrect password when adding items.\n");
		SafeCase3.add(myPen);
		SafeCase3.add(myLaptop);
		SafeCase3.add(myDesignerPen);
		SafeCase3.add(myBomb);
		System.out.print("\n\n");

	}
}
